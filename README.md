# relex (a <u>re</u>gex <u>lex</u>er)

This crate provides several utilities for creating regex-based lexers. A lexer uses RegexSet's
from the regex crate for maximum efficiency. Use this when you want to spin up a lexer quickly.

Here is a quick example to get you started:
```rust
use relex::*;

#[derive(Debug, Clone, PartialEq)]
enum MyToken {
  Whitespace,
  ID,
  Float,
  Eof,
  Unrecognized,
}
impl TokenKind for MyToken {
  fn eof() -> Self { MyToken::Eof }
  fn unrecognized() -> Self { MyToken::Unrecognized }
}

let lex = LexerBuilder::new()
  .token(Rule::new(MyToken::Whitespace, r"\s+").unwrap().skip(true))
  .token(Rule::new(MyToken::ID, r"[A-Za-z]+").unwrap())
  .token(Rule::new(MyToken::Float, r"(\d+)(?:\.(\d+))?").unwrap().capture(true)) // this one captures groups
                                                                                 // because it calls `.capture(true)`
  .build();

let mut scanner = lex.iter(" abc", 0);
let token = scanner.next().unwrap();
assert_eq!(token.kind, MyToken::ID);
assert_eq!(token.text, "abc");
assert_eq!(token.skipped[0].kind, MyToken::Whitespace);
```
## Collecting Code Coverage

```sh
❯ cargo test
    Finished test [unoptimized + debuginfo] target(s) in 0.01s
     Running unittests (target/debug/deps/relex-f41d18d0b3597fac)
...                     ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^-- note this executable and then run:
❯ kcov target/cov --include-pattern=relex/src ./target/debug/deps/relex-f41d18d0b3597fac
...
❯ open target/cov/index.html
```

## License (MIT)

The MIT License (MIT)

Copyright © 2021 Jonathan Apodaca <jrapodaca@gmail.com>

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the “Software”), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

